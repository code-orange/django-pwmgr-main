from celery import shared_task

from django_pwmgr_models.django_pwmgr_models.models import *


@shared_task(name="pwmgr_sync_mdat_to_tenants")
def pwmgr_sync_mdat_to_tenants():
    known_tenants = PwmgrTenantSettings.objects.all()
    known_customer_ids = list()

    for known_tenant in known_tenants:
        known_customer_ids.append(known_tenant.customer.id)

    new_mdat_customers = MdatCustomers.objects.filter(enabled=True).exclude(
        id__in=known_customer_ids
    )

    for new_mdat_customer in new_mdat_customers:
        new_mdat_customer_tenant = PwmgrTenantSettings(
            customer=new_mdat_customer,
        )
        new_mdat_customer_tenant.save()

    return
