from django.core.management.base import BaseCommand

from django_pwmgr_main.django_pwmgr_main.tasks import pwmgr_sync_mdat_to_tenants


class Command(BaseCommand):
    help = "Run task pwmgr_sync_mdat_to_tenants"

    def handle(self, *args, **options):
        pwmgr_sync_mdat_to_tenants()
        self.stdout.write(self.style.SUCCESS("Successfully finished."))
